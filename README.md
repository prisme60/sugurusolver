# Suguru Solver

The goal is to solve a Suguru puzzle.

WIP : an automatic solver for this android game :
[https://play.google.com/store/apps/details?id=com.alexuvarov.android.suguru](https://play.google.com/store/apps/details?id=com.alexuvarov.android.suguru)

## Grid format :
Each case [value, letter]
  --> value can be empty (space) or a digit
   --> letter is corresponding to the group id (a => 0, b => 1, ...)

## Grid example (3x3): 
```
  a1a b
 2a b3b
 1c2c c 
```