use std::{
    fmt,
    result::Result,
    cmp::PartialEq,
    collections::HashSet
};

type ValueType = usize;
struct Value(HashSet<ValueType>);

// refers to some case in Data::cases
#[derive(Clone, Copy, PartialEq)]
pub struct CaseIndex(usize);
// refers to some group in Data::groups
#[derive(Clone, Copy, PartialEq)]
pub struct GroupIndex(usize);

pub struct Grid {
    cases: Vec<Case>,
    groups: Vec<Group>,
    dimensions: (usize, usize),
}

pub struct Case {
    group_id: GroupIndex,
    value: Value,
    coord: (usize, usize),
}

pub struct Group {
    id: GroupIndex,
    case_ids: Vec<CaseIndex>,
}

impl Grid {
    pub fn new() -> Self {
        Grid {
            cases: vec![],
            groups: vec![],
            dimensions: (0, 0),
        }
    }

    pub fn get_case_index_from_coord(&self, x: usize, y: usize) -> CaseIndex {
        CaseIndex(x * self.dimensions.0 + y)
    }

    pub fn fill(&mut self, _path: &str) -> Option<()> {
        self.dimensions = (3, 3);
        let vals = concat!(
            " a1a b\n",
            "2a b3b\n",
            "1c2c c\n"
        );
        let mut it = vals.chars();
        //TODO Read file

        for x in 0..self.dimensions.0 {
            for y in 0..self.dimensions.1 {
                let val = it.next()?;
                let g = it.next()?;
                self.add_case(x,y,g, val);
            }
            let endline = it.next()?;
            if endline != '\n' {
                panic!(format!("Carriage return expected {0}", x));
            }
        }

        self.fill_empty_hashsets_with_all_cases();
        Some(())
    }

    /// **Init method helper**
    /// Add a case to the grid
    ///
    fn add_case(&mut self, x: usize, y: usize, group_char: char, val_char : char) {
        let group_id = Group::get_index_from_char(group_char);
        let case_id = self.get_case_index_from_coord(x, y);
        let value = Value::get_value_from_char(val_char);

        if  group_id.0 >= self.groups.len() {
            // group not yet added, so create it and attach it with its first case
            self.groups
                .push(Group::new(group_id, case_id));
        }
        else {
            // add the case_id to the existing group
            self.groups[group_id.0].case_ids.push(case_id);
        }
        // Create the case
        self.cases.push(Case::new(group_id, value,(x, y)));
    }

    /// **Init method helper**
    /// Fill empty hashsets of each cases with all possibility.
    ///
    /// This method is use only once during initialization.
    fn fill_empty_hashsets_with_all_cases(&mut self) {
        for g in &self.groups {
            let max_id = g.case_ids.len();
            for case_id in &g.case_ids {
                let c = &mut self.cases[case_id.0];
                if c.value.0.is_empty() {
                    c.value.0.extend(1..max_id);
                }
            }
        }
    }

    /// Filter cases.
    ///
    /// A number must appear only one time in a group
    /// If a case has been determined, other case of the group should remove it from their possible values (hashset)
    pub fn filter_cases(&mut self) {
        for g in &self.groups {
            for case_id in &g.case_ids {
                let c = &self.cases[case_id.0];
                if let Some(v) = c.determined_value() {
                    for case_id_to_update in &g.case_ids {
                        if case_id_to_update != case_id {
                            self.cases[case_id_to_update.0].value.remove_value(v);
                        }
                    }
                }
            }
        }
    }

    pub fn print(&self) {
        let mut current_x = 0 as usize;
        for case in &self.cases {
            if current_x < case.coord.0 {
                //new line
                current_x = case.coord.0;

            }
        }
    }
}

impl fmt::Display for Grid {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        let mut current_x = 0 as usize;
        for case in &self.cases {
            if current_x < case.coord.0 + 1{
                //new line
                current_x = case.coord.0 + 1;
                write!(f, "{}", match case.determined_value(){
                    Some(v) => format!("\n|{0}{1}|",v,Group::get_char_from_index(case.group_id)),
                    None => format!("\n| {0}|",Group::get_char_from_index(case.group_id))
                })?;
            }
            else {
                //same line
                write!(f, "{}", match case.determined_value(){
                    Some(v) => format!("{0}{1}|",v,Group::get_char_from_index(case.group_id)),
                    None => format!(" {0}|",Group::get_char_from_index(case.group_id))
                })?;
            }
        }
        Ok(())
    }
}

impl Group {
    pub fn new(group_id: GroupIndex, case_id: CaseIndex) -> Self {
        Group {
            id: group_id,
            case_ids: vec![case_id],
        }
    }

    /// Static method
    fn get_index_from_char(char_group: char) -> GroupIndex {
        match char_group {
            'a'..='z' => GroupIndex(char_group as usize - 'a' as usize),
            'A'..='Z' => GroupIndex(char_group as usize - 'A' as usize),
            _ => panic!("Invalid char group"),
        }
    }

    fn get_char_from_index(group_index:GroupIndex) -> char {
        match group_index.0 {
            0..=26  => (group_index.0 + 'a' as usize) as u8 as char,
            _ => panic!("Invalid char group"),
        }
    }
}

impl Case {
    fn new(group_id: GroupIndex, value: Value, coord: (usize, usize)) -> Self {
        Case {
            group_id,
            value,
            coord,
        }
    }

    fn determined_value(&self) -> Option<ValueType> {
        if self.value.0.len()==1 {
            if let Some(v) = self.value.0.iter().next() {
                return Some(*v);
            }
        }
        None
    }
}

impl Value {
    /// Static method
    fn get_value_from_char(char_val: char) -> Value {
        match char_val {
            '1'..='9' => Value(vec![char_val as usize - '0' as usize]
                .into_iter()
                .collect()),
            ' ' | _ => Value(HashSet::new()),
        }
    }

    /// Remove v from the possible values
    fn remove_value(&mut self, v: ValueType) {
        self.0.remove(&v);
    }
}