pub mod grid;
use grid::Grid;

// Try to make an autmatic solver for :
// https://play.google.com/store/apps/details?id=com.alexuvarov.android.suguru
//
//grid format :
// each case [value, letter]
//   --> value can be empty (space) or a digit
//   --> letter is corresponding to the group id (a => 0, b => 1, ...)
//
// grid example (3x3):
//  a1a b
// 2a b3b
// 1c2c c

fn main() {
    let mut grid = Grid::new();
    grid.fill("grid.txt");
    println!("Before filter :\n{0}",grid);
    grid.filter_cases();
    println!("After filter :\n{0}", grid);
}
